using System.Collections.Generic;

namespace Uno.Client.Common
{
    public class RoomData
    {
        public string Id { get; set; } = "";

        public string Name { get; set; } = "TestRoom";

        public int TotalPlayers { get; set; } = 4;

        public List<RoomPlayerData> PlayersConnected { get; set; } = new List<RoomPlayerData>(4);
    }
}