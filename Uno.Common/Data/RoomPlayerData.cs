using System.ComponentModel.DataAnnotations;

namespace Uno.Client.Common
{
    public class RoomPlayerData
    {
        [Required] public string PlayerId { get; set; } = "A1C12051-962F-4C05-B82E-934721041F6C";

        [Required] public string UserName { get; set; } = "TestUser";

        [Required] public int ConnectionStatus { get; set; } = 0;
    }
}