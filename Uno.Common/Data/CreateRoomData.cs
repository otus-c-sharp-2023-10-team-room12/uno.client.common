﻿using System.ComponentModel.DataAnnotations;

namespace Uno.Client.Common
{
    public class CreateRoomRequest
    {
        [Required(ErrorMessage = "Room Name cannot be empty!")]
        public string Name { get; set; }

        [Range(2, 4, ErrorMessage = "Wrong Min Max Player Limit, must be min:2,max:4")]
        public int MaxPlayers { get; set; }

        [Required] public RoomPlayerData? Creator { get; set; }
    }
}