﻿namespace Uno.Common.DTO.GamePlay
{
    /// <summary>
    /// Сыграть карту
    /// </summary>
    public class PlayCardRequest
    {
        /// <summary>
        /// Айди пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Айди карты
        /// </summary>
        public int CardId { get; set; }
    }
}
